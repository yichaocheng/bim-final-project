import tensorflow as tf

from keras.engine import  Model
from keras.layers import Flatten, Dense, Input
from keras_vggface.vggface import VGGFace
from keras_vggface import utils
from keras import losses

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions
from keras.models import load_model

from sklearn.preprocessing import MinMaxScaler
import numpy as np
from image_reader import ReadImageFiles
import math

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.5
session = InteractiveSession(config=config)

#Some code from https://github.com/rcmalli/keras-vggface
nb_class = 512
hidden_dim = 1024

'''
vgg_model = VGGFace(include_top=False, input_shape=(224, 224, 3))
last_layer = vgg_model.get_layer('pool5').output
x = Flatten(name='flatten')(last_layer)
x = Dense(hidden_dim, activation='relu', name='fc6')(x)
x = Dense(hidden_dim, activation='relu', name='fc7')(x)
out = Dense(nb_class, name='fc8')(x)
model = Model(vgg_model.input, out)
'''

model = load_model('model_drop_50/model-003.h5')

img = load_img('../blonde_crop.png', target_size=(224, 224))
x = img_to_array(img)
x = np.expand_dims(x, axis=0)
x = utils.preprocess_input(x, version=1) # or version=2
preds = model.predict(x)

Y = np.load("../generated_images_10k/code_numpy.npy")
scaler = MinMaxScaler()
scaler.fit(Y)
preds = scaler.inverse_transform(preds)

np.save("../blonde_crop_code.npy", preds)
print(preds)

