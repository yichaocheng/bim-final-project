import tensorflow as tf

from keras.engine import  Model
from keras.layers import Dropout, Flatten, Dense, Input
from keras_vggface.vggface import VGGFace
from keras_vggface import utils
from keras import losses

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import decode_predictions

from sklearn.preprocessing import MinMaxScaler
import numpy as np
from image_reader import ReadImageFiles
import math

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from keras.regularizers import l1


config = ConfigProto()
#config.gpu_options.allow_growth = True
config.gpu_options.per_process_gpu_memory_fraction = 0.5
session = InteractiveSession(config=config)

#Some code from https://github.com/rcmalli/keras-vggface
nb_class = 512
hidden_dim = 1024

vgg_model = VGGFace(include_top=False, input_shape=(224, 224, 3))
last_layer = vgg_model.get_layer('pool5').output
x = Flatten(name='flatten')(last_layer)
x = Dense(hidden_dim*3, activation='relu', activity_regularizer=l1(0.001), name='fc6')(x)
x = Dense(hidden_dim, activation='relu', activity_regularizer=l1(0.001), name='fc7')(x)
x = Dropout(0.2)(x)
out = Dense(nb_class, name='fc8')(x)
model = Model(vgg_model.input, out)


for layer in model.layers:
    if layer.name != "fc6" and layer.name !=  "fc7" and layer.name != "fc8":
        layer.trainable = False


N = 20000
filenames = ['../generated_images_20k_1024/image_'+str(i)+'.png' for i in range(N)]

image_height = image_width = 224
image_channels = 3

X = np.empty((N, image_height, image_width, image_channels), dtype=np.float32)
Y = np.load("../generated_images_20k_1024/code_numpy.npy")
scaler = MinMaxScaler()
scaler.fit(Y)
Y = scaler.transform(Y)

import cv2
for i in range(N):
    if i % 2000 == 0:
        print(i)
    image = load_img(filenames[i], target_size=(224, 224))
    image = img_to_array(image)
    image = image.reshape((1, 224, 224, 3))
    image = utils.preprocess_input(image, version=1)
    X[i,:,:,:] = image


train_count = int(math.floor(N * 0.9))

X_train = X[0:train_count]
Y_train = Y[0:train_count]

X_test = X[train_count: ]
Y_test = Y[train_count: ]

from keras import optimizers
from keras.callbacks import ModelCheckpoint

model.compile(loss=losses.mean_squared_error, optimizer=optimizers.Adam())
checkpoint = ModelCheckpoint('model_20k/model-{epoch:03d}.h5',
                                 monitor='val_loss',
                                 verbose=0,
                                 save_best_only='true',
                                 mode='auto')
model.fit(X_train, Y_train, batch_size=25, epochs=10, verbose=1, shuffle=True, validation_data=(X_test, Y_test), callbacks=[checkpoint])

