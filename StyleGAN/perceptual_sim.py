import torch
import torch.nn as nn
import torch.optim as optim

from torchvision import datasets, models, transforms
from torchvision.utils import make_grid, save_image

import numpy as np
from model import StyledGenerator
import torchvision.models as models
from PIL import Image
import math

def normalize_batch(batch):
    # normalize using imagenet mean and std
    mean = batch.new_tensor([0.485, 0.456, 0.406]).view(-1, 1, 1)
    std = batch.new_tensor([0.229, 0.224, 0.225]).view(-1, 1, 1)
    batch = batch.div_(255.0)
    return (batch - mean) / std

class Vgg16(torch.nn.Module):
    def __init__(self):
        super(Vgg16, self).__init__()
        vgg16 = models.vgg16(pretrained=True)
        features = list(vgg16.features)[:23]
        self.features = nn.ModuleList(features).eval()

    def forward(self, x):
        results = []
        for ii,model in enumerate(self.features):
            x = model(x)
            if ii in {3,8,15,22}:
                results.append(x)

        return results


device = 'cuda'

import os

image = Image.open('aligned_images/girl_crop.png').convert('RGB')
image = image.resize((224,224), Image.ANTIALIAS)
image = transforms.ToTensor()(image)
image = transforms.Lambda(lambda x: x.mul(255))(image)
target_image = torch.autograd.Variable(image).to(device)
target_image.requires_grad = True


generator = StyledGenerator(512).to(device)
generator.load_state_dict(torch.load('stylegan-256px-new.model')['g_running']) # this model was too big to include
generator.eval()

mean_style = torch.autograd.Variable(torch.load("generated_images_10k/mean_style.pt"))
step = int(math.log(256, 2)) - 2
mean_style.requires_grad = True

#generate guess
source_code = torch.autograd.Variable(torch.randn(1,512)).to(device)
source_code.requires_grad = True

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
vgg_model = Vgg16().to(device)

target_image = target_image.unsqueeze(0)
target_image = normalize_batch(target_image)
target_feat = vgg_model(target_image)

optimizer = optim.Adam([source_code], lr=1.)
mse_loss = nn.MSELoss()


for i in range(2000):

    #generate image
    source_image = generator(
            source_code, step=step, alpha=1, mean_style=mean_style, style_weight=0.7
        )
    #rescale image
    source_image = make_grid(source_image, nrow=1, padding=2, pad_value=0, normalize=True, range=(-1,1), scale_each=False)
    #crop to 244x244
    source_image = source_image[:, 16:-16, 16:-16]
    source_image = source_image.unsqueeze(0)
    #rescale 
    source_image = source_image.mul(255)
    source_image = source_image.add_(0.5).clamp_(0, 255)
    #normalize
    source_image = normalize_batch(source_image)
    #get visual features
    source_feat = vgg_model(source_image)

    optimizer.zero_grad()

    #loss metrics
    loss = mse_loss(source_feat[0], target_feat[0]) + mse_loss(source_feat[1], target_feat[1]) + mse_loss(source_feat[2], target_feat[2]) + mse_loss(source_feat[3], target_feat[3])
    #loss = mse_loss(source_feat[0], target_feat[0])
    #loss = mse_loss(source_feat, target_feat)
    
    if i%100 == 0:
        print(loss.item())
        save_image(source_image, "temp/percept"+str(i)+".png", normalize=True, range=(-1, 1))
    
    grad = torch.autograd.grad(loss, source_code)
    source_code.grad = grad[0]

    optimizer.step()


#generate final image
source_image = generator(
            source_code, step=step, alpha=1, mean_style=mean_style, style_weight=0.7
        )

save_image(source_image, "percept.png", normalize=True, range=(-1, 1))
torch.save(source_code, "girl_code.pt")


