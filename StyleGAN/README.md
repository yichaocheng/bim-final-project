# StyleGAN Implementation 

### What do the files do? 

The files in this directory contains the Pytorch implementation for StyleGAN. In order to use this implementation, start a new conda / virtualenv environment and install the modules in the `requirements.txt` .

`model.py` contains the models necessary for training (`train.py`)the StyleGAN model

`generate.py` produces style-mixed images given pre-trained models and latent code for the two input images

`perceptual_sim.py` optimizes latent code given a cropped image (i.e. from `aligned_images`)

The folder `old_mapping.py` has code for the first attempt to map image to latent code using a feedforward network with a VGGFace top. Unfortunately, the results were not as good as the perceptual loss.

### Training StyleGAN

To train StyleGAN, use `train.py` and fill in parameters accordingly. Some benchmark times given GPU resources are listed in the [original implementation](https://github.com/NVlabs/stylegan). 

Due to the size of the model, it is not included in this Gitlab repository. See [256x256 Model](https://drive.google.com/open?id=1QlXFPIOFzsJyjZ1AtfpnVhqW4Z0r8GLZ) for the link to the pre-trained model used for finding perceptual similarity.

### Transforming input image

0. Train or download pre-trained StyleGAN model - remainder of code currently works for 256x256 shape image.
1. Add input image to aligned_images, change input name in `perceptual_sim.py`.
2. Run `perceptual_sim.py` to generate latent code for input image.
3. Run `generate.py` to mix image styles.


### Acknowledgement 

We would like to thank professor Marynel Vázquez for a great 459 course experience. We would also like to acknowledge open source code for [StyleGAN](https://github.com/rosinality/style-based-gan-pytorch). 

