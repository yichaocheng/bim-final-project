#!/usr/bin/env python3

import numpy as np
import pandas as pd
import random
import cv2
import tensorflow as tf
import datetime
import os
from mtcnn.mtcnn import MTCNN
from keras_vggface.vggface import VGGFace
from keras_vggface.utils import preprocess_input
from keras_vggface.utils import decode_predictions
from keras import backend as K

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

import sys
import dlib

# create a vggface model
vggFace = VGGFace(model='vgg16')

def prepare_dataset():
	images_folder = 'celeba-dataset/img_align_celeba/img_align_celeba'
	df_attr = pd.read_csv('celeba-dataset/list_attr_celeba.csv')
	no_glasses = df_attr['Eyeglasses']<0
	no_hat = df_attr['Wearing_Hat']<0
	young = df_attr['Young']>0
	df_young = df_attr[young & no_hat & no_glasses]
	df_young = df_young[['image_id']]

	# split data into train, val, and test
	input_train, input_test = split_data(df_young['image_id'].values.reshape((df_young.shape[0],1)), 0.6)
	input_val, input_test = split_data(input_test, 0.5)

	# load pics into .npy file
	get_pics_input(images_folder, 'fast-neural-style-keras/input_train/train', input_train)
	get_pics_input(images_folder, 'fast-neural-style-keras/input_test/train', input_test)
	get_pics_input(images_folder, 'fast-neural-style-keras/input_val/train', input_val)

def split_data(input_data, train_percentage):
	indices = list(range(input_data.shape[0]))
	np.random.shuffle(indices)

	num_train = int(input_data.shape[0]*train_percentage)
	train_indices = indices[:num_train]
	test_indices = indices[num_train:]

	return input_data[train_indices,:], input_data[test_indices,:]

def get_pics_input(folder, dest_folder, input_train):
	for i in range(input_train.shape[0]):
		print(i)
		pic_name = input_train[i][0]
		filename = '{}/{}'.format(folder, pic_name)
		try:
			img = get_face(filename)
			img = cv2.resize(img, (224, 224), cv2.INTER_LINEAR)
			cv2.imwrite('{}/{}'.format(dest_folder, pic_name), img)
		except:
			continue

# extract a single face from a given photograph
def extract_face(pic_array, required_size=(224, 224)):
	try:
		# create the detector, using default weights
		detector = MTCNN()
		# detect faces in the image
		results = detector.detect_faces(pic_array)
		# extract the bounding box from the first face
		x1, y1, width, height = results[0]['box']
		x2, y2 = x1 + width, y1 + height
		# extract the face
		face = pixels[y1:y2, x1:x2]
		# resize pixels to the model size
		image = cv2.resize(face, required_size, cv2.INTER_LINEAR)
		print("YAY")
		return image
	except:
		return cv2.resize(pic_array, required_size, cv2.INTER_LINEAR)

def get_face(img_filename):
	detector = dlib.get_frontal_face_detector()
	img = cv2.imread(img_filename)
	# The 1 in the second argument indicates that we should upsample the image
	# 1 time.  This will make everything bigger and allow us to detect more
	# faces.
	dets = detector(img, 1)
	return img[dets[0].top():dets[0].bottom(), dets[0].left():dets[0].right()]


def deprocess(img_filename):
	img = cv2.imread(img_filename) * 255.0
	cv2.imshow('', img)
	cv2.waitKey(0)

# put data into .npy files for faster loading
prepare_dataset()