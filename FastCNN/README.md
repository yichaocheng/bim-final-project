# Fast Feedforward Neural Style Transfer

This is the feedforward, convolutional implementation of the image transformation network that attempts to transfer stylistic features associated with aging across pictures.

## Running the Model
### System Requirements
This module runs on Ubuntu 18.04 with Python 3.6.8. Please make sure all dependencies in the *requirements.txt* file are installed.

### Processing the Training Dataset
Download the CelebA Dataset from Kaggle. Make sure the paths in *prepare\_dataset()* inside *prepare\_dataset.py* are as desired. Run the following command to extract the faces from the pictures into the specified destination folder.
```bash
$ ./prepare_dataset.py
```

### The Config File
See *old.json* for reference. Contains hyperparameters that can be modified to tune network performance.

### Training the Model
```bash
$ python main.py -c old.json -m train
```

### Generating Output Images From the Model
```bash
$ python main.py -c old.json -m predict -i input_face.jpg -o output_face.jpg
```
## Sample Results
With VGG16 Network and adjusted hyperparameters:
![FastCNN VGG16 Results](fastCNN16Results.png)

With VGGFace Network:
![FastCNN VGGFace Results](fastCNNFaceResults.png)

## References 

[github inspiration](https://github.com/overflocat/fast-neural-style-keras)

