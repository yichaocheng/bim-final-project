import tensorflow as tf 
from tensorflow.keras.layers import Conv2D, Conv2DTranspose, LeakyReLU, Input, Activation, Concatenate
from tensorflow.keras.initializers import RandomNormal
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model, load_model
# import matplotlib
# matplotlib.use("agg")
import matplotlib.pyplot as plt
from keras.engine import InputSpec
from keras.engine.topology import Layer
from keras.preprocessing.image import load_img, img_to_array, array_to_img
  
# config =  tf.compat.v1.ConfigProto() 
# config.gpu_options.allow_growth = True
# makes sure that if the machine supports GPU, use it. 
physical_devices = tf.config.experimental.list_physical_devices('GPU')
tf.config.experimental.set_visible_devices(physical_devices, 'GPU')
import numpy as np 
import os
import random
import datetime
# pip install git+https://www.github.com/keras-team/keras-contrib.git
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
import tensorflow.keras.backend as K 
from model import *
from process_images import ImageGenerator
import argparse

class CycleGAN():
	# faces_all, faces_female, faces_male 
	def __init__(self, dataset_name, n_epochs, logs_dir, use_resnet, batch_size):
		self.input_shape = (256, 256, 3)
		self.alpha = 0.2 
		self.batch_size = batch_size
		self.dataset_name = dataset_name 
		self.n_epochs = n_epochs 
		self.logs_dir = logs_dir

		# define the generators 
		if use_resnet:
			self.g_AtoB = build_resnet_generator(9, self.input_shape)
			self.g_BtoA = build_resnet_generator(9, self.input_shape)
		else:
			self.g_AtoB = build_unet_generator(self.input_shape)
			self.g_BtoA = build_unet_generator(self.input_shape) 
		# self.g_AtoB.summary()

		# define the discriminators 
		self.d_A = build_discriminator(self.input_shape)
		self.d_B = build_discriminator(self.input_shape)
		# self.d_A.summary()

		# define the composite models 
		# composite: A -> B -> [real/fake, A]
		self.c_model_AtoB = composite_model(self.g_AtoB, self.d_B, self.g_BtoA, self.input_shape)
		# composite: B -> A -> [real/fake, B]
		self.c_model_BtoA = composite_model(self.g_BtoA, self.d_A, self.g_AtoB, self.input_shape)

		# initialize image generator class, load the image filenames in each domain into memory. 
		self.image_generator = ImageGenerator(batch_size, self.logs_dir, dataset_name, self.input_shape)
		print ("Finished building all models and the image generator")

	def train(self):

		n_patch = self.d_A.output_shape[1]
		batch_size = self.batch_size

		# fake image pools 
		poolA, poolB = list(), list()
		batch_per_epoch = int(len(self.image_generator.train_A_dataset) / self.batch_size)

		# calculate the total number of batches  
		n_steps = batch_per_epoch * self.n_epochs
		
		# batch_size = 1 or 2 typically 
		# batch_size unchanged 

		valid = np.ones((batch_size, n_patch, n_patch, 1))
		fake = np.zeros((batch_size, n_patch, n_patch, 1))

		sample_interval = 3000 # 3000, 5000 
		fd = open(os.path.join(self.logs_dir, 'output.txt'), 'w')
                

		for i in range(1, n_steps+1):
			# select a batch of real samples
			A_imgs = self.image_generator.sample_real_images_A(batch_size)
			B_imgs = self.image_generator.sample_real_images_B(batch_size)
			
			# predictions 
			fake_A_imgs = self.g_BtoA.predict(B_imgs)
			fake_B_imgs = self.g_AtoB.predict(A_imgs)

			# update fakes from pool
			fake_A_imgs = fake_image_pool(poolA, fake_A_imgs)
			fake_B_imgs = fake_image_pool(poolB, fake_B_imgs)

			# update generator B->A via GAN and cycle consistency loss
			g_loss2, _, _, _, _  = self.c_model_BtoA.train_on_batch([B_imgs, A_imgs], [valid, A_imgs, B_imgs, A_imgs])
			# update discriminator for A -> [real/fake]
			dA_loss_real,dA_acc_real = self.d_A.train_on_batch(A_imgs, valid)
			dA_loss_fake,dA_acc_fake = self.d_A.train_on_batch(B_imgs, fake)
			# update generator A->B via GAN and cycle consistency loss
			g_loss1, _, _, _, _ = self.c_model_AtoB.train_on_batch([A_imgs, B_imgs], [valid, B_imgs, A_imgs, B_imgs])
			# update discriminator for B -> [real/fake]
			dB_loss_real,dB_acc_real = self.d_B.train_on_batch(B_imgs, valid)
			dB_loss_fake,dB_acc_fake = self.d_B.train_on_batch(A_imgs, fake)
			# write per batch summary to output.txt in the log directory, so that we can plot accuracy and loss graphs later. 
			fd.write('Epoch:{},Batch:{},dA_loss_real:{},dA_acc_real:{},dB_loss_fake:{},dB_acc_fake:{},g_loss1:{},g_loss2:{}\n'.format(int(i//batch_per_epoch) ,int(i % batch_per_epoch),dA_loss_real, dA_acc_real, dB_loss_fake, dB_acc_fake,g_loss1, g_loss2 ))

			print ("[Epoch {}/{}] [Batch {}/{}] [DA loss: {}, {}] [DB loss: {}, {}] [G loss: {}, {}]"
				.format(int(i//batch_per_epoch), self.n_epochs, int(i % batch_per_epoch), batch_per_epoch, dA_loss_real, 
					dA_loss_fake, dB_loss_real, dB_loss_fake, g_loss1, g_loss2))
			# save checkpoints (model + sample images) at intervals. 
			if (i % sample_interval == 0):
				self.image_generator.checkpoint(self.g_AtoB, self.g_BtoA, 4, int(i % batch_per_epoch), int(i//batch_per_epoch))
		fd.close()
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--dataset_name', help="name of dataset", type=str, default='faces_all')
	parser.add_argument('--epochs', help="number of epochs for training", type=int, default='50')
	parser.add_argument('--logs_dir', help="logs_directory", type=str, default="")
	parser.add_argument('--use_resnet', help="generator model selection", type=int, default='1')
	parser.add_argument('--batch_size', help="batch size for training", type=int, default='100')
	args = parser.parse_args()
	if len(args.logs_dir) == 0:
		args.logs_dir = 'logs/log_{}'.format(datetime.datetime.now().strftime("%m-%d-%Y-%H-%M"))
	if not os.path.isdir(args.logs_dir):
		os.makedirs(args.logs_dir)

	model = CycleGAN(args.dataset_name, args.epochs, args.logs_dir, args.use_resnet, args.batch_size)
	model.train()
	sys.exit(0)

