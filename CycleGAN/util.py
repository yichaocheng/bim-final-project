import tensorflow as tf 
import numpy as np 
import os
import time 
import random 
from keras.preprocessing.image import load_img, img_to_array, array_to_img

# general utils which we didn't end up using. 
def load_images(directory):
	# load images and resize them to 256 x 256 
	img_list = list()
	if (not directory) or directory[-1] != '/':
		directory += '/' 

	for f in os.listdir(directory):
		img = img_to_array(load_img(directory+f, ))
		img_list.append(img)
		print (img.shape)
	print ("loaded {} images from directory {}.".format(len(img_list), directory))

	return img_list 

def normalize_data_per_row(data, normal_range=False):
    """
    Normalize a give matrix of data (samples must be organized per row)
    :param data: input data
    :return: normalized data with pixel values in [0,1]
    """
    assert len(data.shape) == 4, "Expected the input data to be a 4D matrix"

    if normal_range:
    	normalized_data = data.astype('float32') / float(255)
    else:
    	normalized_data = (data.astype('float32') - 127.5)/ float(255)

    assert data.shape == normalized_data.shape, "data after normalization should have the same shape"
    return normalized_data 

def split_data(input, target, train_percent):
    """
    Split the input and target data into two sets
    :param input: inputs [Nx2] matrix
    :param target: target [Nx1] matrix
    :param train_percent: percentage of the data that should be assigned to training
    :return: train_input, train_target, test_input, test_target
    """
    assert input.shape[0] == target.shape[0], \
        "Number of inputs and targets do not match ({} vs {})".format(input.shape[0], target.shape[0])

    indices = range(input.shape[0])
    np.random.shuffle(indices)

    num_train = int(input.shape[0]*train_percent)
    train_indices = indices[:num_train]
    test_indices = indices[num_train:]

    return input[train_indices, :], target[train_indices,:], input[test_indices,:], target[test_indices,:]



