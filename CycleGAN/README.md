# CycleGAN Implementation 

### What do the files do? 

The files in this directory contains the tensorflow implementation for CycleGAN, as well as the pre-trained models. In order to use this implementation, start a new conda / virtualenv environment and install the modules in the `requirements.txt` .

`main.py` is the driver for training the model for given datasets with directory structure `datasets/[dataset name]/trainA` and  `datasets/[dataset name]/trainB`. 

`model.py` implements the discriminator, generator, and composite models. 

`process_images.py` contains various utils for image pre-processing and handling, used both before and during model training. 

`parse_output.py` parses the `logs/logdir/output.txt` file produced during training to plot the loss function graphs and accuracy graphs. 

`test.py` is the driver for testing a pretrained model on a set of images or just a single image. 

`util.py` is not used for this project, and it contains some utility functions. 

### Training process 

Training the CycleGAN models is a heavily memory intensive process, so do not try to train on a regular computer with no GPU. For this project, we trained our model on Google Cloud by using a deep learning virtual machine (via ssh, scp). In order to enable the GPU, we installed tensorflow with conda and trained within a conda environment. 

Saving the models at interval (see `main.py`) and producing the checkpoint images proved to be tremendously helpful. 

### Pre-trained models 

1. Under `pretrained_models/`, we saved our _best_ pretrained models that were using different datasets or different loss weights. 

2. `AtoB_male_female.h5/BtoA_male_female.h5` are models trained with using the [**CelebA Dataset**](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html), where for image domain `A=young` and `B=old` we included both female and male images, and _young/old_ is filtered by a binary label. 

3. `AtoB_male_loss1.h5/BtoA_male_loss1.h5` are models trained using only the male images from the [**IMDB-WIKI Dataset**](https://data.vision.ee.ethz.ch/cvl/rrothe/imdb-wiki/) where we combined the faces only images from both IMDb and Wiki. Here, `A=young` only contains images with age in range  [0, 30], and `B=old` only contain images in range [60, 100]. 

4. `AtoB_male_loss2.h5/BtoA_male_loss2.h5` and `AtoB_male_loss3.h5/BtoA_male_loss3.h5` are models trained using the same dataset as in `loss1`, but with different loss weights. That is, during CycleGAN training, the generator is updated using four losses, the identity loss, the adversarial loss, the forward cycle consistency loss, and the backward cycle consistency loss. (see `composite_model` in `model.py`) And these three sets of models exeprimented with different relative weights of the four losses ([1, 5, 10, 10] for 2., and 3., [5, 1, 10, 10] for `loss2`, and [5, 5, 10, 10] for `loss3`).  

### Acknowledgement 

We would like to thank professor Marynel Vázquez for all her help and invaluable feedback throughout the project, and for providing the Google Cloud credits that were absolutely essential. We would also like to thank the following authors for generously sharing valuable educational content on the web: [original implementation of CycleGAN](https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix), [machine learning mastery](https://machinelearningmastery.com/), [pix2pix implementation for reference](https://github.com/eriklindernoren/Keras-GAN/blob/master/pix2pix/pix2pix.py). 

