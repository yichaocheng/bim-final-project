import numpy as np 
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import scipy.io 
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img 
from keras.preprocessing.image import ImageDataGenerator 
import os 
import io 
import random 
import csv 
import shutil 
import tensorflow as tf 
from glob import glob 
# from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
import tensorflow.keras.backend as K
import pandas as pd 

# utility function for translating the datenum into years, used for calculating age
def datenum_to_datetime(datenum):
	days = datenum % 1
	hours = days % 1 * 24
	minutes = hours % 1 * 60
	seconds = minutes % 1 * 60
	exact_date = datetime.fromordinal(int(datenum)) \
		+ timedelta(days=int(days)) + timedelta(hours=int(hours)) \
		+ timedelta(minutes=int(minutes)) + timedelta(seconds=round(seconds)) \
		- timedelta(days=366)
	return exact_date.year 



###############################################################################
# Some stats:
# Total number of images: 202599, after filtering out hats and glasses: 180989
# Filtered images female: 112712, male: 68277 
###############################################################################
# utility function run to filter images and partition data into separate folders.
def preprocess_dataset(path, csv_filename, test):
	df = pd.read_csv(os.path.join(path, csv_filename))

	# print (df.columns.values) 
	if test:
		df = df.sample(100)
	# only get no Eyeglasses and no Wearing_Hat 
	neg_label = ['Eyeglasses', 'Wearing_Hat']
	f_df = df[df['Eyeglasses']!=1]
	filtered_df = f_df[f_df['Wearing_Hat']!=1]
	female_df = filtered_df[filtered_df['Male'] != 1]
	male_df = filtered_df[filtered_df['Male'] == 1]

	# 6 different dataframes 
	all_young_df = filtered_df[filtered_df['Young']==1] 

	all_old_df = filtered_df[filtered_df['Young']!=1]

	female_young_df = female_df[female_df['Young']==1]

	female_old_df = female_df[female_df['Young']!=1]

	male_young_df = male_df[male_df['Young']==1]

	male_old_df = male_df[male_df['Young']!=1]

	# make directories
	dir_list = ['all_young', 'all_old', 'female_young', 'male_young', 'female_old', 'male_old']
	# saves these images to respective folders 
	df_list = [all_young_df, all_old_df, female_young_df, male_young_df, female_old_df, male_old_df]
	for d in dir_list:
		if not os.path.exists(d):
			os.mkdir(d)

	for i, df in enumerate(df_list):
		for img_id in df['image_id']:
			shutil.copy2('celeba-dataset/img_align_celeba/img_align_celeba/{}'.format(img_id), dir_list[i])

	for d in dir_list:
		print ("Saved {} images to directory {}".format(len(os.listdir(d)), d))


def process_imdb_wiki(name):
	if name == "imdb":
		mat = scipy.io.loadmat('imdb_crop/imdb.mat')
		input = mat['imdb'][0][0][0].shape[1]
		columns = ["dob", "photo_taken", "full_path", "gender", "name", "face_location", "face_score", "second_face_score", "celeb_names", "celeb_id"]
	elif name == "wiki":
		mat = scipy.io.loadmat('wiki_crop/wiki.mat')
		input = mat['wiki'][0][0][0].shape[1]
		columns = ["dob", "photo_taken", "full_path", "gender", "name", "face_location", "face_score", "second_face_score"]

	df = pd.DataFrame(index = range(0,input), columns = columns)
	data = mat[name][0][0]
	for j in range(len(data)):
		df[columns[j]] = pd.DataFrame(data[j][0])

	# filters out invalid / bad data 
	df = df[df['dob'] > 10000] 
	df = df[df['face_score'] != -np.inf]
	df = df[df['face_score'] >= 3]
	df = df[df['second_face_score'].isna()]
	df = df[~df['gender'].isna()]
	# to confirm the dataset 
	df.gender.unique()
	df.face_score.unique()
	df.second_face_score.unique()

	# get the birth year and calculate age from it. 
	df['birth_year'] = df['dob'].apply(datenum_to_datetime)
	df['age'] = df['photo_taken'] - df['birth_year']
	df = df[df['age'] <= 100]
	df = df[df['age'] > 0]
	female_df = df[df['gender']==0.0]
	male_df = df[df['gender']==1.0]
	female_young_df = female_df[female_df['age']<=30]
	male_young_df = male_df[male_df['age']<=30]
	female_old_df = female_df[female_df['age']>=60]
	male_old_df = male_df[male_df['age']>=60]
	dir_list = [name+'_female_young_df', name+'_male_young_df', name+'_female_old_df', name+'_male_old_df']
	df_list = [female_young_df, male_young_df, female_old_df, male_old_df]

	for d in dir_list:
		if not os.path.exists(d):
			os.mkdir(d)

	for i, df in enumerate(df_list):
		for full_path in df['full_path']:
			shutil.copy2('{}_crop/{}'.format(name, full_path[0]), dir_list[i])

	for d in dir_list:
		print ("Saved {} images to directory {}".format(len(os.listdir(d)), d))



# utility function for loading from and compressing images into npz file
# we did not end up using it. 
def load_and_compress_train_images(src_dir, size=(256, 256)):
	# assert that input src dir exists 
	assert os.path.exists(src_dir), "path to load images from {} does not exist!".format(src_dir)
	# define A and B folders 
	train_path_A = os.path.join(src_dir, 'trainA/')
	train_path_B = os.path.join(src_dir, 'trainB/')
	print ("loading from {}".format(train_path_A))

	A_list, B_list = list(), list()

	for f in os.listdir(train_path_A):
		img = img_to_array(load_img(os.path.join(train_path_A, f), target_size=size))
		A_list.append(img)
	for f in os.listdir(train_path_B):
		img = img_to_array(load_img(os.path.join(train_path_B, f), target_size=size))
		B_list.append(img)
	A_list, B_list = np.asarray(A_list), np.asarray(B_list)
	print ("loaded {} A images, and {} B images".format(len(A_list), len(B_list)))

	np.savez_compressed(os.path.join(src_dir, 'training_images.npz'), trainA=A_list, trainB=B_list)
	print ("Saved dataset: {} to {}".format('training_images.npz', src_dir))

# load images from npz file 
def load_images_from_npz(filename, size=(256, 256), normalize = True):
	assert os.path.exists(filename), "path to load images from {} does not exist!".format(src_dir)
	images = np.load(filename)
	trainA, trainB = images['trainA'], images['trainB']

	print ("loaded {} A images, and {} B images".format(len(trainA), len(trainB)))
	# to validate  
	plt.imshow(trainA[0].astype('uint8')) 
	plt.axis('off')
	plt.show()

"""
image generator class, used during training to generate batches of images from domain X and Y. 
this solves the problem that the dataset is way too big to be entirely kept in memory, by only
keeping in memory the names of the images and only loading one batch at a time into memory. 
"""
class ImageGenerator():
	# directory structure is datasets/dataset_name/type/*.jpg 
	def __init__(self, batch_size, logs_dir, dataset_name='faces', image_size=(256, 256)):
		self.batch_size = batch_size
		self.image_size = image_size
		self.dataset_name = dataset_name
		self.A_train_path = 'datasets/{}/trainA/'.format(dataset_name)
		self.B_train_path = 'datasets/{}/trainB/'.format(dataset_name)
		self.train_A_dataset = os.listdir(self.A_train_path)
		self.train_B_dataset = os.listdir(self.B_train_path)
		self.logs_dir = logs_dir

	def sample_real_images_B(self, n_samples):
		img_idx = np.random.randint(0, len(self.train_B_dataset), n_samples)
		X = list()
		for ind in img_idx:
			# print (self.B_train_path+self.train_B_dataset[ind])
			img = self.read_img(os.path.join(self.B_train_path, self.train_B_dataset[ind]))
			X.append(img)
		# normalize to [-1, 1]
		X = np.asarray(X) /127.5-1 
		# y = np.zeros((len(X), n_patches, n_patches, 1)) 
		return X

	def sample_real_images_A(self, n_samples):

		img_idx = np.random.randint(0, len(self.train_A_dataset), n_samples)
		X = list()
		for ind in img_idx:
			# print (self.A_train_path+self.train_A_dataset[ind])
			img = self.read_img(os.path.join(self.A_train_path, self.train_A_dataset[ind]))
			X.append(img)
		# normalize to [-1, 1]
		X = np.asarray(X) /127.5-1 
		# y = np.zeros((len(X), n_patches, n_patches, 1)) 
		return X

	def sample_fake_images(self, data, g_model):
		# generate fake instance
		X = g_model.predict(data)
		return X 

	def read_img(self, filename):
		assert os.path.exists(filename), "Error: file {} to load does not exist".format(filename)
		img = img_to_array(load_img(filename, target_size=self.image_size))
		img = img.astype('float32')
		return img

	# summarize performance, save checkpoints 
	def checkpoint(self, g_AtoB, g_BtoA, n_samples, batch_it, epoch_it):

		A_imgs = self.sample_real_images_A(n_samples)
		fake_A_imgs = g_AtoB.predict(A_imgs)
		B_imgs = self.sample_real_images_B(n_samples)
		fake_B_imgs = g_BtoA.predict(B_imgs)

		# back to normal 
		A_imgs = (A_imgs+1) * 127.5
		B_imgs = (B_imgs+1) * 127.5
		# fake_A_imgs = (fake_A_imgs+1) * 127.5
		# fake_B_imgs = (fake_B_imgs+1) * 127.5
		all_imgs = np.concatenate([A_imgs, fake_A_imgs, B_imgs, fake_B_imgs])

		titles = ['Real A', 'Fake B', 'Real B', 'Fake A']

		fig, axs = plt.subplots(4, n_samples)
		cnt = 0 
		for i in range(4):
			for j in range(n_samples):
				axs[i, j].imshow(all_imgs[cnt].astype('uint8'))
				axs[i, j].set_title(titles[i])
				axs[i, j].axis('off')
				cnt += 1
		fig.savefig("{}/{}_{}.png".format(self.logs_dir, epoch_it, batch_it))
		plt.close()
		g_AtoB.save('{}/{}_{}_trained_AtoB.h5'.format(self.logs_dir, epoch_it, batch_it))
		g_BtoA.save('{}/{}_{}_trained_BtoA.h5'.format(self.logs_dir, epoch_it, batch_it))

	# simulating an epoch 
	def generate_batch_train(self, batch_size, list_type):

		n_batches = int(n_images / batch_size)
		if list_type == 'trainA':
			image_list = os.listdir(self.path_train_A)
		elif list_type == 'trainB':
			image_list = os.listdir(self.path_train_B)
		elif list_type == 'testA':
			image_list = os.listdir(self.path_test_A)
		else:
			image_list = os.listdir(self.path_test_B)

		for i in range(n_batches-1):
			batch = image_list[i*batch_size:(i+1)*batch_size]
			img_list = list()
			for img in batch:
				img = self.read_img(img)
				img_list.append(img) 

			img_list = np.asarray(img_list)/127.5-1.

			yield img_list




