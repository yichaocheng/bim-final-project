from keras.layers import Conv2D, Conv2DTranspose, LeakyReLU, Input, Activation, Concatenate
from keras.initializers import RandomNormal
from keras.optimizers import Adam
from keras.models import Model 
from keras.engine import InputSpec
from keras.engine.topology import Layer
from keras.preprocessing.image import load_img, img_to_array, array_to_img
import tensorflow as tf 
# set this if your machine does not have GPU 
# tf.config.experimental.set_visible_devices([], 'GPU')
import numpy as np 
import os
import random
import datetime
# pip install git+https://www.github.com/keras-team/keras-contrib.git
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
import tensorflow.keras.backend as K 

# https://stackoverflow.com/questions/50677544/reflection-padding-conv2d
class ReflectionPadding2D(Layer):
    def __init__(self, padding=(1, 1), **kwargs):
        self.padding = tuple(padding)
        self.input_spec = [InputSpec(ndim=4)]
        super(ReflectionPadding2D, self).__init__(**kwargs)

    def compute_output_shape(self, s):
        """ If you are using "channels_last" configuration"""
        return (s[0], s[1] + 2 * self.padding[0], s[2] + 2 * self.padding[1], s[3])

    def call(self, x, mask=None):
        w_pad,h_pad = self.padding
        return tf.pad(x, [[0,0], [h_pad,h_pad], [w_pad,w_pad], [0,0] ], 'REFLECT')


''' Saves the generated images to corresponding pool of images, such that it
	keeps filling up the pool until it reaches max size of 50. After which, 
	it randomly selects one to replace with a new image from images.'''
def fake_image_pool(fake_pool, images, max_size=50):

	selected = list()
	for img in images:
		if len(fake_pool) < max_size:
			# simply add the image to the fake_pool 
			fake_pool.append(img)
			selected.append(img)
		elif random.uniform(0, 1) < 0.5:
			# replace an existing image and use replaced image
			ix = random.randint(0, len(fake_pool)-1)
			selected.append(fake_pool[ix])
			fake_pool[ix] = img
			selected.append(img)
		else:
			# use image, but don't add it to the fake_pool
			selected.append(img)
	return np.asarray(selected)

'''	composite model to update generator with both adversarial loss and cyc loss 
	g_model_1 -> d_model -> g_model_2'''
def composite_model(g_model_1, d_model, g_model_2, image_shape):
	# only train g_model_1 (A -> B and B -> A) 
	g_model_1.trainable = True
	d_model.trainable = False
	g_model_2.trainable = False

	input = Input(shape=image_shape)
	fake_1 = g_model_1(input)
	output_d = d_model(fake_1)
	# identity element
	input_id = Input(shape=image_shape)
	output_id = g_model_1(input_id)
	# forward cycle
	output_f = g_model_2(fake_1)
	# backward cycle
	gen2_out = g_model_2(input_id)
	output_b = g_model_1(gen2_out)
	# define model graph
	model = Model([input, input_id], [output_d, output_id, output_f, output_b])

	# compile model with weighting of least squares loss and L1 loss
	model.compile(loss=['mse', 'mae', 'mae', 'mae'], 
				loss_weights=[1, 5, 10, 10], 
				optimizer=Adam(lr=0.0002, beta_1=0.5))

	return model


def conv_layer(n_filters, kern_size, strides, alpha, padding, input_layer, is_norm, activation):

	# Conv2D(filters=64, kernel_size=(4,4), strides=2, padding='same', kernel_initializer=kern_init)(input)
	kern_init = RandomNormal(stddev = 0.02)
	l = Conv2D(filters=n_filters, kernel_size=kern_size, strides=strides, padding=padding, kernel_initializer=kern_init)(input_layer)

	if is_norm:
		l = InstanceNormalization(axis=-1)(l) 
	if activation == 'leaky':
		l = LeakyReLU(alpha=alpha)(l)
	else:
		l = Activation(activation)(l)
	return l 

def conv_transpose_layer(n_filters, kern_size, strides, alpha, padding, input_layer, is_norm, activation):
	kern_init = RandomNormal(stddev = 0.02)
	l = Conv2DTranspose(filters=n_filters, kernel_size=kern_size,strides=strides, padding=padding, kernel_initializer=kern_init)(input_layer)
	if is_norm:
		l = InstanceNormalization(axis=-1)(l) 
	if activation == 'leaky':
		l = LeakyReLU(alpha=alpha)(l)
	else:
		l = Activation(activation)(l)
	return l 


def build_discriminator(input_shape):
	kern_init = RandomNormal(stddev = 0.02)
	input = Input(shape=input_shape) 
	# first layer, without instance normalization 

	l = conv_layer(64, (4,4), 2, 0.2, 'same', input, False, 'leaky')
	l = conv_layer(128, (4,4), 2, 0.2, 'same', l, True, 'leaky')
	l = conv_layer(256, (4,4), 2, 0.2, 'same', l, True, 'leaky')
	l = conv_layer(512, (4,4), 2, 0.2, 'same', l, True, 'leaky')
	l = conv_layer(512, (4,4), 1, 0.2, 'same', l, True, 'leaky')

	out_l = Conv2D(filters=1, kernel_size=(4,4), strides=1, padding='same', kernel_initializer=kern_init)(l)

	model = Model(input, out_l)

	model.compile(loss='mse', optimizer=Adam(lr = 0.0002, beta_1=0.5),
              metrics=['accuracy'], loss_weights=[0.5])
	return model

# unet generator, used in pix2pix 
def build_unet_generator(input_shape):
	kern_init = RandomNormal(stddev = 0.02)

	def unet_generator(nf, input_layer, batchnorm):
		d = Conv2D(nf, (4,4), 2, padding='same', kernel_initializer=kern_init)(input_layer)
		d = LeakyReLU(alpha=0.2)(d)
		if batchnorm:
			d = BatchNormalization(momentum=0.8)(d)
		return d

	def unet_decoder(nf, input_layer, skip_layer, dropout=True):
		d = Conv2DTranspose(nf, (4,4), 2, padding='same', kernel_initializer=kern_init)(input_layer)
		d = BatchNormalization()(d, training=True)
		if dropout:
			d = Dropout(0.5)(d, training=True)
		d = Concatenate()([d, skip_layer])
		l = Activation('relu')(d)
		return d 

	input = Input(shape=input_shape) 
	e1 = unet_generator(64, input, False)
	e2 = unet_generator(128, e1, True)
	e3 = unet_generator(256, e2, True)
	e4 = unet_generator(512, e3, True)
	e5 = unet_generator(512, e4, True)
	e6 = unet_generator(512, e5, True)
	e7 = unet_generator(512, e6, True)
	# output 
	b = Conv2D(512, (4,4), 2, padding='same', kernel_initializer=kern_init)(e7)
	b = Activation('relu')(b)

	u1 = unet_decoder(512, b, e7, True)
	u2 = unet_decoder(512, u1, e6, True)
	u3 = unet_decoder(512, u2, e5, True)
	u4 = unet_decoder(512, u3, e4, False)
	u5 = unet_decoder(256, u4, e3, False)
	u6 = unet_decoder(128, u5, e2, False)
	u7 = unet_decoder(64, u6, e1, False)

	output = Conv2DTranspose(3, (4, 4), 2, padding='same', kernel_initializer=kern_init)(u7)
	output = Activation('tanh')(output)

	model = Model(input, output)
	return model 

# resnet generator, used in cyclegan 
def build_resnet_generator(n_blocks, input_shape):
	kern_init = RandomNormal(stddev = 0.02) 
	def resnet(nf, input_layer):
		padded_input_layer = ReflectionPadding2D()(input_layer)
		l = Conv2D(nf, (3,3), padding='valid', kernel_initializer=kern_init)(padded_input_layer)
		l = InstanceNormalization(axis=-1)(l)
		l = Activation('relu')(l)

		padded_l = ReflectionPadding2D()(l)
		l = Conv2D(nf, (3,3), padding='valid', kernel_initializer=kern_init)(padded_l)
		l = InstanceNormalization(axis=-1)(l)
		l = Concatenate()([l, input_layer])
		return l 
	input = Input(shape=input_shape)

	l = conv_layer(64, (7,7), 1, 0.2, 'same', input, True, 'relu')
	l = conv_layer(128, (3,3), 2, 0.2, 'same', l, True, 'relu')
	l = conv_layer(256, (3,3), 2, 0.2, 'same', l, True, 'relu')

	for i in range(n_blocks):
		l = resnet(256, l)

	l = conv_transpose_layer(128, (3,3), 2, 0.2, 'same', l, True, 'relu')
	l = conv_transpose_layer(64, (3,3), 2, 0.2, 'same', l, True, 'relu')
	l_out = conv_layer(3, (7,7), 1, 0.2, 'same', l, True, 'tanh')

	model = Model(input, l_out)
	return model 





