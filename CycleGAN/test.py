from keras.models import Model, load_model
# import matplotlib
# matplotlib.use("agg")
import matplotlib.pyplot as plt
from keras.preprocessing.image import load_img, img_to_array, array_to_img
import tensorflow as tf
# only set this if you are sure your machine doesn't have GPU on it. 
tf.config.experimental.set_visible_devices([], 'GPU')
import numpy as np 
import os
# to get rid of the warnings 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import logging
logger = tf.get_logger()
logger.setLevel(logging.ERROR)
import sys
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import keras
sys.stderr = stderr
# pip install git+https://www.github.com/keras-team/keras-contrib.git
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
import tensorflow.keras.backend as K 
from model import * 
from process_images import ImageGenerator 

# https://fengl.org/2014/07/09/matplotlib-savefig-without-borderframe/ 
# remove borders 
def save_image(data, cm, fn):
   
    sizes = np.shape(data)
    height = float(sizes[0])
    width = float(sizes[1])
     
    fig = plt.figure()
    fig.set_size_inches(width/height, 1, forward=False)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
 
    ax.imshow(data, cmap=cm)
    plt.savefig(fn, dpi = height) 
    plt.close()

# generate and quickly show one image on the screen 
def gen_save_img(model, image_path):
	pixels = img_to_array(load_img(image_path, target_size=(256, 256)))
	pixels = pixels.astype('float32')
	pixels = pixels/127.5 -1.0
	pixels = np.expand_dims(pixels, 0)
	gen_image = model.predict(pixels)
	gen_image = (gen_image + 1)*127.5
	plt.imshow(gen_image[0].astype('uint8'))
	plt.axis('off')
	plt.show()

# generate fake images for all images in input_img_path, save to output_img_path
def generate_fake_imgs(model_path, input_img_path, output_img_path):
	assert os.path.exists(input_img_path), "input image path {} does not exist".format(input_img_path) 
	assert os.path.exists(output_img_path), "output image path {} does not exist".format(output_img_path)

	model = model = load_model(model_path, custom_objects={'InstanceNormalization':InstanceNormalization, 'ReflectionPadding2D': ReflectionPadding2D})

	input_lst = os.listdir(input_img_path)
	all_images = [img_to_array(load_img(os.path.join(input_img_path, img), target_size=(256, 256))).astype('float32') for img in input_lst]
	all_images = np.asarray(all_images)
	all_images = all_images / 127.5 -1.0
	
	gen_images = model.predict(all_images)
	for i, gen_img in enumerate(gen_images):
		gen_img = (gen_img + 1)*127.5
		plt.imshow(gen_img.astype('uint8'))
		plt.axis('off')
		# prefix 'gen_' for the generated fake images. 
		plt.savefig(os.path.join(output_img_path, 'gen_'+input_lst[i]), bbox_inches='tight')
		plt.close()

if __name__ == "__main__":
	assert len(sys.argv) == 4, "please provide, in order, model_path, input_dir, output_dir"
	model_path = sys.argv[1]
	input_img_path = sys.argv[2]
	output_img_path = sys.argv[3]
	generate_fake_imgs(model_path, input_img_path, output_img_path)


