import numpy as np 
import sys

import matplotlib.pyplot as plt 

# functions that process the output.txt produced by the training process to plot loss and accuracy graphs 
def plot_loss_fxn(file_path):
	fd = open(file_path, 'r')
	lines = fd.readlines()
	# parse the lines 
	steps = [i for i in range(len(lines))]
	g_loss1, g_loss2 = list(), list()
	for line in lines:
		loss1 = (line.split(',')[6]).split(':')[1]
		loss2 = (line.split(',')[7]).split(':')[1]
		g_loss1.append(float(loss1))
		g_loss2.append(float(loss2))
	dA_loss, dB_loss = list(), list()
	for line in lines:
		lossa = (line.split(',')[2]).split(':')[1]
		lossb = (line.split(',')[4]).split(':')[1]
		dA_loss.append(float(lossa))
		dB_loss.append(float(lossb))
	fig, axs = plt.subplots(2, 2)
	axs[0,0].plot(steps, dA_loss)
	axs[0,0].set_title('dA loss')
	axs[0,1].plot(steps, dB_loss)
	axs[0,1].set_title('dB loss')
	axs[1,0].plot(steps, g_loss1)
	axs[1,0].set_title('generator A loss')
	axs[1,1].plot(steps, g_loss2)
	axs[1,1].set_title('generator B loss')

	# for ax in axs:
	# 	ax.flat(xlabel='steps', ylabel='loss')
	# 	ax.label_outer()

	plt.show()
	fd.close()

def plot_acc_fxn(file_path):
	fd = open(file_path, 'r')
	lines = fd.readlines()
	steps = [i for i in range(len(lines))]
	dA_acc, dB_acc = list(), list()

	for line in lines:
		a_acc = (line.split(',')[3]).split(':')[1]
		b_acc = (line.split(',')[5]).split(':')[1]
		dA_acc.append(float(a_acc))
		dB_acc.append(float(b_acc))
	fig, (ax1, ax2) = plt.subplots(2)
	# print (dA_acc)
	ax1.plot(steps, dA_acc)
	ax1.set_title('dA accuracy')
	ax2.plot(steps, dB_acc)
	ax2.set_title('dB accuracy')
	ax2.set(xlabel='steps', ylabel='acc')
	ax1.set(ylabel='acc')

	plt.show()
	fd.close()

if __name__ == "__main__":
	if len(sys.argv) < 3: 
		print ("please specify the function to plot, and the file path for the input file")
	is_acc_fxn = (int(sys.argv[1]) == 1)
	file_path = sys.argv[2] 
	if is_acc_fxn:
		plot_acc_fxn(file_path)
	else:
		plot_loss_fxn(file_path) 

