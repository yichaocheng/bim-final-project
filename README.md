# Style Transformation on Human Faces using Feedforward and Generative Methods

Final project for Yale CPSC 459/559, Building Interactive Machines, by Annie Gao, Valerie Chen, and Yichao Cheng. 

### Outline 

The goal of this project is to explore automatic transformation of features in human face images for the purposes of interactive applications and demonstrations. In particular, we explored three architectures for human face aging, including Fast Style Transfer [1], Cycle GAN [2], and Style GAN [3]. 

This GitLab repo contains the source code for our implementations, trained models, filtered datasets, and results (images and loss graphs).  

### How to Use 

The three architecture implementations are under `FastCNN/`, `CycleGAN/`, and `StyleGAN/` subdirectories respectively. There is a separate `README` under each subdirectory explaining the details of implementation and usage, as well as potential issues related to environment setup for training and testing and the workarounds employed. 

### Dataset

1. [**IMDB-WIKI-500k+ face images with age and gender labels**](https://data.vision.ee.ethz.ch/cvl/rrothe/imdb-wiki/) - one of the largest datasets of face images with age and gender labels, including 460k+ face images from 20k+ celebrities from IMDb and from Wikipedia. We used the faces only datasets for both IMDb and Wiki. 
2. [**CelebFaces Attributes Dataset (CelebA)**](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html) - a large-scale face attributes dataset with **200K+** celebrity images, each with **40** attribute annotations. The images in this dataset cover large pose variations and background clutter. We downloaded the dataset [**here**](https://www.kaggle.com/jessicali9530/celeba-dataset). 

### Results 

`questionaire_evaluation.csv` contains our questionaire responses, where we asked participants to answer the following questions in response to 6 pairs of `young (original) -> old` images, 2 from each of our models: 

1. Guess the age of the original face?
2. How realistic is the modified face on a scale of 1 to 5? 
3. Guess the age of the modified face?

Here is a summary of the 17 responses we gathered in total: 

<img src="imgs/questionaire_summary.png"  width="600">

### Division of Labor

Here is a breakdown of individual roles within the project:

* Annie: FastCNN implementation, evaluation data analysis
* Yichao: CycleGAN implementation, video editing, code repository management
* Valerie: StyleGAN implementation, web mockup
* All: evaluation survey and response collection, video, presentation slides, paper writing

### Video 

[here](https://gitlab.com/yichaocheng/bim-final-project/blob/master/video.mp4) is our supplementary video, where we briefly explain the three architectures and provide a demo for how our project may be used for interactive applications. 

### Reference 

1. Perceptual losses for real-time style transfer and super-resolution. [link](https://link.springer.com/chapter/10.1007/978-3-319-46475-6_43 )
2. Unpaired Image-to-Image Translation. [link](http://openaccess.thecvf.com/content_ICCV_2017/papers/Zhu_Unpaired_Image-To-Image_Translation_ICCV_2017_paper.pdf)
3. A Style-Based Generator Architecture for Generative Adversarial Networks. [link](http://openaccess.thecvf.com/content_CVPR_2019/papers/Karras_A_Style-Based_Generator_Architecture_for_Generative_Adversarial_Networks_CVPR_2019_paper.pdf)

[1]:https://link.springer.com/chapter/10.1007/978-3-319-46475-6_43	"Perceptual losses for real-time style transfer and super-resolution"
[2]: http://openaccess.thecvf.com/content_ICCV_2017/papers/Zhu_Unpaired_Image-To-Image_Translation_ICCV_2017_paper.pdf	"Unpaired Image-to-Image Translation"
[3]: http://openaccess.thecvf.com/content_CVPR_2019/papers/Karras_A_Style-Based_Generator_Architecture_for_Generative_Adversarial_Networks_CVPR_2019_paper.pdf	"A Style-Based Generator Architecture for Generative Adversarial Networks"

**In conclusion, our main takeaway was**...

<img src="imgs/meme.png" width="350">